![Logo](media/oy-logo.png)

# insert-system-name-here

Software Architecture documentation for system **insert-system-name-here**.

Version 0.0.1 - yyyy-mm-dd

| Authors   | Contact information |
|------------|------------------------|
| Antti Juustila | firstname.lastname at oulu.fi |


**Table of Contents**

[TOC]


---

# Introduction

This document descibes and models the software architecture of *XXX*.

The document is intented to be read by all the stakeholders of the *XXX* development. 

Identify and describe the larger **context** of the development:

* The organization(s) participating in the development.    
* The possible larger system the software developed is a part of.
* What is the purpose of this document.

---

# Environment

This chapter discusses the business goals, stakeholders and development environment related issues.


## Business goals

**Why** this software should be build? 

What **benefits** will it provide and to whom, when it is deployed and used?

Are there any goals that are relevant for the *developing organization*? For example, is there a motivation to create reusable components, frameworks or product line architectures? Are there existing solutions of this kind that are used here in design of this architecture?  


## Stakeholders

**List and shortly describe** the roles of different stakeholders relevant for the development:

* Client(s), and possibly clients of clients 
* Users or user groups, 
* Developer organizations, 
* Subcontractors, consultants, 
* Others.


## Development environment

Describe the target system characteristics as well as the development environment characteristics (as necessary):

* Machine architecture (x86, ARM; desktop, laptop, tablet and/or phones; embedded devices?)
* Network characteristics
* Operating system(s)
* Other relevant environmental characteristics
    * Especially if the system is installed in exceptional environment (outside, subject to weather conditions; space,...)

These are relevant considering, for example, portability requirements. These aspects can also limit the choices related to external libraries to use, as well as the selection of programming languages, after the architectural design.

---

# Architecturally Significant Requirements

These are usually extracted from the requirements analysis documents, if requirements analysis has been conducted in the process.

Otherwise, the requirements should be analysed and defined as part of the architectural design process, incrementally.

## Functional requirements

Major functionality of the system that must be delivered in order to provide value in use.

Usually a list or a table is a good format for representing these. For example,

| Requirement ID | Description |
|-------------------|---------------|
| UR-021             | Member of the club must be able to join a team using the MyClub app. |

Each requirement should be described in more detail in a separate document or documents. Do link these requirements here to that document or documents. 


## Non-functional requirements

Major non-functional requirements that must be delivered.

Usually a list or a table is a good format for representing these. For example,

| Requirement ID | Description |
|-------------------|---------------|
| NFR-011           | All data transfer between the MyClub app and the server must done **securely** over HTTPS. |
| NFR-052           | Domain model should be generic to enable **reusability** of the model in other than sports club contexts (e.g. Martta societies). |

Each requirement should be described in more detail in a separate document or documents. Do link these requirements here to that document or documents.

## Constraints

Technical or non-technical constraints that limit the design and implementation.

May include, for example:

* Memory usage restrictions
* CPU characteristics
* Constraints related to battery powered devices (such as phones)
* Restrictions related to using external components (open source or proprietary)
* Legal and/or business related restrictions

---

# Major Architectural Solutions

What are the most important design solutions used to support the realization of the non-functional requirements. These include:

1. Architectural style(s),
1. Design patterns,
1. Idioms,
1. Techiques such as configuration, composition, inheritance,
1. Other,

...which support the design and implementation of the non-functional requirements in the architecture.

**List** these here and **explain in 1-3 sentences**. The solutions are described in detail below in the various UML models and textual documentation of those in Architectural views chapter.

For example:

| Solution | Description       |
| --------------|---------------|
| MVC           | Model-View-Controller enables separation of UI from logic to support *portability* of the MyClub app. |


---

# Architectural views

The order of the subchapters here depends on the software characteristics. Sometimes, for example, the Deployment view is not relevant since the software consists of one component deployed on a single machine. It still could be modelled and included here, but **may be moved** to the end. More important models are presented **first**.

Remember that while models are developing, you may need to add new information to other models while working on one model. Managing model images from the modeling tool to this document may bee too laborous. Consider to **include the models as images** in this document **only after you feel the models are not changing so often any more** to avoid unnecessary work.

When identifying a need for an interface class in a class model, you may want to add that interface also the component where the interface class will be placed when building the system. When, in a sequence diagram, you see that an object needs to send a message to another object, it may require adding the message as a method to the receiving class. And if the object is sending a message to another, it requires some kind of a dependency between the classes in the class diagram. So all the models are being modified and added to, while working on any of them.

If the UML diagrams in the sections below are large, you can **shrink the image** to a smaller one, embed it to the document and **provide a link** to the full resolution image. Or split the image to several parts, if that works better, and explain the parts separately. 

You may also link to the UML tool's model file (e.g. Visual Paradigm model), but make sure readers who do not have that tool installed, can still view the models in this document as images.

Remember also that the various views are *views* to the **one and the same** software. The views are linked and interdependent. So do refer from one view documentation to other views to help the reader to find the relevant information. 

For example, when you have a use case in use case view named "Start the app", and if you have modeled what happens when the app starts up using an interaction diagram (sequence diagram), refer to that sequence diagram from the use case model -- and vice versa. Or, if you mention in your class diagram that some classes are relevant in the configuration of the component, which happens at app launch, refer from the class diagram explanation to the sequence diagram in Use case view -- reader can then see from there how this configuration functionality is executed. 

**Use links** to help the reader jump to the other diagram/chapter to ease navigation in the document. It is very rare that anyone prints these documents, but read them online. Utilize the linking and embedding to assist the reader in navigation.

---

## Use case view

**Provide** the UML **Use case diagram** and **include descriptions** of that diagram here. Main goal is to provide a view to the main functionality and features of the system.

This is important in getting the overview of the features of the system, as well as communicating this with the stakeholders, especially clients and users.

### Actors

**List the actors**, both human and non-human. Describe shortly the characteristics of the actors, and their interest in the software. Describe shortly **how the actor communicates** with the use cases (is it a GUI, or a network link (e.g. external systems)).

| Actor | Short description |
|-------|----------------------|
|Member | Member of a society (e.g. sports club, Martat) |



### UC-001 -- Use cases, one subchapter for each

If use cases are long and are documented in detail, with step by step scenarios etc., consider creating a [separate document](SWAUseCaseTemplate.md) for each use case and link them here. If you do so, do **describe the use case shortly** here also, since we do not want to write clickbait documentation. Consider the readers' needs.

Use, **UML activity diagrams** to illustrate what happens in one scenario (usually the default scenario without error and exeption handling) -- what steps are taken to achieve the goal of the user in the use case.

Often, **sequence diagrams** are also used to describe **how** application level **objects interact** to provide the end result of the use case to the user. Usually, these are modeled after the class diagrams are developed (but not yet necessarily ready). 

It is, however, also possible to first draw interation diagrams with objects that have no class, and then use this to identify which kind of classes would be needed. Then you can use sequence diagrams to create the necessary classes to the class diagram. Which approac to use depends on the situation and personal preferences.

---

## Component view

If the sofware contains of many components and/or it is using external components, describe the structure using an **UML component diagram**.

Make sure component **dependencies** are shown. Model the **stereotypes** (library, executable, file,...) and **interfaces** (required, offered) of the components when applicable.

Make sure the **role of each component is explained** -- *why* it is used (external components) and/or why it exists as a separate component (components of this software).

---

## Deployment view

Include UML **Deployment model(s)** and textual **explanations** of those, describing the deployment of the software components and other artifacts in the environment where the software is installed. Deployment models are often examples, since deployment can be done in different ways in different contexts.

If there are any optional configurations (e.g. optional components to be installed), that are architecturally relevant, point those out using UML Notes or Collaboration symbols. Write explanations in the document. Explanations should always explain *why*.

If necessary, model more than one deployment diagram, if there are significantly different situations or possibilities in deploying the software.

---

## Logical view

Logical view models the software **static structure**, using UML **packages** and **class diagrams**.

### Overview

Show the logical high level structure using **packages** and their **dependencies**. Explain *why* the package structure is what it is. Is the package there because of:

* reusability; it will also used in other contexts, or 
* it is an element developed by us in another project/context,
* division of work; package is designed and implemented by one indivudual or group,
* external development; package is an external library or developed by a subcontractor,
* ... something else.

In the same way, explain *why* packages *depend* on other packages; what they need from the other package.

Provide a subchapter for each of the packages using the structure below.

### Package xxxx

Describe each package's purpose, role in the software. *Why* it is needed? Are there any non-functional requirements the package supports?

Show the **class diagram** of the package. Explain the major important features or functionality of the package. Describe the core classes of the package and their roles in the software.

Keep the focus on the **structure**, static properties of the classes and the **roles** and high level **collaboration** between the classes. Leave out explaining the detailed functionality, since that should be explained elsewhere in Process view and Use case view (and associated activity and interaction diagrams there).

If there are some very simple and low level "utility" classes, they can be either explained very shortly or ignored in the text. Focus on architecturally **relevant** classes, and to those classes which are participating in delivering some important **non-functional requirement** (e.g. modifiability, extensibility, security, configurability or performance).

---

## Process View

Describe how the sofware is structured at **runtime** -- what *processes* and possibly secondary *threads* of execution are created when the software is executed and *why*? What asynchronous operations are executed? *Why* they need to be asynchronous?

Use **UML component diagram** (or **Deployment diagram**, if the system is distributed) to model the process/thread structure. Create a component diagram where the component stereotypes used are a `<<process>>` for a process, obviously, and `<<thread>>` for a thread inside a process. Use the UML composition association to show what threads a process "owns" and optionally, which objects (classes) own the threads.

**Explain** each thread in detail: what it does, does it collaborate with other threads by invoking them or is it being invoked by them? What inter thread mechanisms are used in thread collaboration? For example, are queues used to pass data? Are locks and/or condition variables used to wait for permission to continue to execute, or to awaken threads to do their job.

Use **UML activity diagrams** and/or state chart diagrams to model the behaviour of the threads or the lifetime of a class with complicated behaviour. Also sequence diagrams can be used to describe the functionality in the threads.

---

## Data View

Data view illustrates the types and the structure of the **data** the software processes. This includes, for example:

* Database(s):
    * Entity-Relationship diagrams, describing the tables and relations between the data elements,
* Text and/or binary data files, whether input or output files:
    * Structure and format; for text files, is the data comma or tab separated (csv, tsv), or structured in some other way. What is the format of binary files?
* Configuration files, if any
    * What is the structure? Key-value? Text, property file, JSON, yaml? What are compulsory configuration data files, which are optional? What are the legal values?
* If software is networked, in which format the data is exchanged over the network?
    * Describe the data structures and formats used

You should indicate the data and/or configuration files or databases used by the software in a component diagram as components or artefacts. Refer to that diagram from there to help the user to understand which software components use what data components.

It is also important to model at which point(s) in the software architecture the **externalization and internalization** of the data happens -- from the external format (JSON, database, csv,...) to the internal format (objects, binary data structs) and vice versa. This is an essential part of how the **software communicates with the external world**, thus architecturally important.

Refer to the components and/or classes in respective diagrams, which are used to externalize and internalize data (a.k.a. [marshalling](https://en.wikipedia.org/wiki/Marshalling_(computer_science)) or [serializing](https://en.wikipedia.org/wiki/Serialization)). 

If necessary (ie. the process is not trivial), illustrate the process of marshalling using **interaction diagrams**. For example, if the software receives data from the network in JSON, show in an interaction diagram how data is read from a connection by one object, and then internalized from JSON to binary data object -- how different objects/classes in the architecture handle this transformation, starting from a networking object, to the point where the internal data object has been created from the JSON.

Provide syntax and examples of e.g. JSON data structures the software produces or consumes. For example:

```
{
"package": "a6ba159a-4ddf-4006-9357-f99f7be58dbf",
"payload": "{\"events\" : [[\"PlatformSpecific\",1], [\"Updated\",1], [\"IsFile\",1]], \"target\" : \"/Users/juustila/workspace/tmp/file.txt\", \"users\":[\"Antti\"]}",
"type": "data"
}
```

And then explain:

> The file system event JSON message contains:
>
>   * A package id, identifying the package with an UUID,
>   * type, which is always "data",
>   * payload, which contains the file system event details: what event(s) happened, what file was changed and who was the user who changed the file.

If there are many and/or complicated data structures, you may consider a separate document you can use for more detailed documentation. In that case, include here a list of structures and their purposes and links to the detailed data descriptions.


# Analysis

After the architecture **design** is ready or almost ready, it **should be reviewed** to confirm that the solutions in the design do implement the essential functional and especially non-functional requirements. This can be done using architecture evaluation methods, such as ATAM.

This section of the document should summarize the results of the evaluation:

* does the architecture have **risks**; solutions which could lead to unsatisfactory results considering one or more non-functional requirement?
* are there any **sensitivity points**; solutions that are sensitive towards some non-functional requirement in a sense that the solution is removed or changed, it could lead to the requirement not being met?
* are there any **tradeoff points**; solutions that satisfy some requirement but negatively effects some other requirement?
* what are the **non-risks**, solutions which do help meeting requirement(s) and have no other consequences?

# References

Other documents, artifacts, files, models that were used in the design and modelling the arcitecture of the software.

# Appendices

Possible appendices that were not included above but are relevant for the design process and/or the end result.
