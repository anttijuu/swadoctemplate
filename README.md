# README #

This repository contains a document template for writing software architecture documentation. The template is used in the BSc course *Software Architectures*, at [Degree Programme of Information Processing Science](https://www.oulu.fi/tol), University of Oulu, Finland.

To get started, you may either:

1. Clone this repository and start writing your documentation using [Markdown](https://bitbucket.org/tutorials/markdowndemo). Attach images and files, link videos or use whatever media that helps in creating the documentation. 
1. In read mode here, copy the text as HTML to a word processor (MS Word, Apple Pages, OpenOffice Writer, Google Docs, etc.) . Most editors support pasting HTML as formatted text. Continue writing in your preferred editor.

First option, working with Markdown and git, gives you version management and collaboration support "for free". Obviously some of the document editors also support cooperation and versioning.

### How do I get set up? ###

The document template is in file [SWADocTemplate.md](SWADocTemplate.md). Other templates are linked from there.

If writing the documentation as Markdown, you can keep the file and directory structure cleaner if you put all your media files into the [media](media) subdirectory and link them to the text from there.

### Contribution guidelines ###

If you notice opportinities for improvement here, you may

1. fork the project, 
1. create branch for your suggestions, 
1. commit those in the branch and 
1. provide a pull request to this project so I can review and accept them.

Or then just create an issue and explain what is the problem. I'll fix it if necessary, when I have time.

### Who do I talk to? ###

* Antti Juustila, firstname.lastname at oulu dot fi
* [INTERACT Research Unit](https://interact.oulu.fi), University of Oulu, Finland
* [Degree Programme of Information Processing Science](https://www.oulu.fi/tol)
