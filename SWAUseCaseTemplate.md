# Use case UC-001

## Use case name here

Short description of the **goal** of the use case.

## Actors

Actor(s) that participate in the use case.

## Preconditions

What must be there before the use case can be initiated by the actor.

1. Thing 1
1. Thing 2

## Assumptions 

What factors, situational attributes, are assumed (eg. environmental)

1. Assumption 1

## Priority

How important is this use case. Scale could be e.g. 1...5.

Priority can be used in scheduling the development process, by focusing on the higher priority use cases first.

## Frequency

How often this use case is realized, e.g., once a week, 500 times a day, etc.

## Basic procedure

Describe the “normal” processing path, a.k.a. the Happy Path with no error handling or exeptional situations. If needed, these can be described in a separate use case scenario or below.

1.   Use case begins when …
2.   
3.   Use case ends when …

## Alternative procedure(s)

### Alternate Course A

Description of the alternate course of action/steps. If only part of the basic procedure is changing, indicate only those changing or added/removed steps from the basic procedure.

Condition: Indicate what happened
A.6   List the steps

## Post conditions

What are the state of affairs after successfully going through the basic procedure:

1. This is the state of that now.
1. Maybe another state too.


## Notes

List any "to dos" or concerns to be addressed.

Important decisions made during the development of this use case.

